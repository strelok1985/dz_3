//
//  ViewController.swift
//  dz_3
//
//  Created by Andrey Bakanov on 07.03.2019.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //////////////////////////////////////////////
    //DZ 3.1
    func manhCost(){
        let costOfOstrov : Float = 24.0
        let procent : Float = 0.06
        let costOfProcentYear : Float = costOfOstrov * procent
        let firstYear = 1826
        let lastYear = 2019
        var countOfYear = 0
        var sumOfProcent : Float = 0
        var d : Float = 0
        countOfYear = lastYear - firstYear
        for  _ in  0..<countOfYear {
            sumOfProcent = sumOfProcent + costOfProcentYear
        }
           d = sumOfProcent + costOfOstrov
        print ("Sum of procent for \(countOfYear) year =  \(d)")
    }
    /////////////////////////////////////////////
    //DZ 3.2
    func studentOne(){
        let stepuha : Float = 700
        let zatrati : Float = 1000
        let inflacia : Float = 0.03
        let period = 9
        let infoPerMounth : Float = zatrati * inflacia
        var inflacSummMoney : Float = 0
        var inflaciaMoney :Float = 0
        inflacSummMoney = infoPerMounth + zatrati
        
           for  i in  0...period {
            inflaciaMoney = inflaciaMoney  + (( Float(i) * infoPerMounth ) + inflacSummMoney)
             }
        var fianalScore : Float = 0
        fianalScore = inflaciaMoney - (stepuha * Float(period) + stepuha)
       
        print (" studentu nuzno \(fianalScore) chtobi prozit god")
    }
    /////////////////////////////////////////////////////
    //DZ 3.3
    let stepuha : Float = 700
    let inflacia : Float = 0.03
    var period = 0
    
    func studentTwo(){
        
        let zberezenia : Float = 2400
        let zatrati : Float = 1000
        
      
       
        calcRestOfMoney(zberezenia: zberezenia, zatratiSInflacia: zatrati)
        
    }
    func calcRestOfMoney(zberezenia: Float, zatratiSInflacia: Float){
        
        let ostatokBabok = zberezenia - zatratiSInflacia + stepuha
        if ostatokBabok >= 0 {
            period += 1
            print("zivem dalshe, sberezenia: \(zberezenia), zatrati: \(zatratiSInflacia) ")
            let newInflation = zatratiSInflacia + zatratiSInflacia*inflacia
            calcRestOfMoney(zberezenia: ostatokBabok, zatratiSInflacia: newInflation)
        } else {
            print("babok ne xvatilo, prozili \(period)" )
        }
    }
    ////////////////////////////////////////////////////
    //DZ 3.4
    func peremenaChisel(){
        let number = 876
        var firstNumeral = 0
        var sekondNumeral = 0
        var thirdNumeral = 0
        var twoNumeralNumber = 0

        firstNumeral = number / 100
        twoNumeralNumber = number - (firstNumeral*100)
        sekondNumeral = twoNumeralNumber / 10
        thirdNumeral = twoNumeralNumber - (sekondNumeral*10)
        
          print( "number is \(number). backward number \(thirdNumeral)\(sekondNumeral)\(firstNumeral)")
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       //manhCost ()
       //studentOne()
       // studentTwo()
        peremenaChisel()
        
    }


}

